import pytest
from unittest.mock import MagicMock
from assignment.factorial import *


class Testclass:


    @pytest.fixture()
    def obj(self):
        obj = Factorial()
        return obj

    @classmethod
    def setup_class(cls):
        print("\n\nStart of Factorial\n")

    @classmethod
    def teardown_class(cls):
        print("\nEnd of Factorial\n\n")


    @pytest.mark.parametrize("num,result",[(4,24),(2,2)])
    def test_factorial(self, obj,num,result):
        assert obj.fac(num) == result


    def test_Exception_with_bad_int(self,obj):
        with pytest.raises(Exception,match="Negative value"):
            obj.fac(-10)

    def test_return(self,obj,monkeypatch):
        mock_file=MagicMock()
        mock_file.readline = MagicMock(return_value="4")
        mock_open =MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open",mock_open)
        result=obj.readFromFile("_input")
        mock_open.assert_called_once_with("_input","r")
        assert result =="4"

